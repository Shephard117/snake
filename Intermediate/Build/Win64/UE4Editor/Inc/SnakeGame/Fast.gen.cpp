// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Fast.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFast() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AFast_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFast();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AFast::StaticRegisterNativesAFast()
	{
	}
	UClass* Z_Construct_UClass_AFast_NoRegister()
	{
		return AFast::StaticClass();
	}
	struct Z_Construct_UClass_AFast_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFast_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFast_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Fast.h" },
		{ "ModuleRelativePath", "Fast.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AFast_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AFast, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFast_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFast>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFast_Statics::ClassParams = {
		&AFast::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFast_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFast_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFast()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFast_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFast, 1626116065);
	template<> SNAKEGAME_API UClass* StaticClass<AFast>()
	{
		return AFast::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFast(Z_Construct_UClass_AFast, &AFast::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AFast"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFast);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
