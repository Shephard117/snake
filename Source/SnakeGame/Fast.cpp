// Fill out your copyright notice in the Description page of Project Settings.


#include "Fast.h"
#include "Snake.h"

// Sets default values
AFast::AFast()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFast::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFast::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFast::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(0.2f);
			Destroy();
		}
	}
}

