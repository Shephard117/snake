// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Food.h"
#include "Fast.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnake;


UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnake* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnake> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodElementClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	//���������� ��� �� �
	float MinX = -520; float MaxX = 520;
	//���������� ��� �� Y
	float MinY = -1000; float MaxY = 1000;
	//������������� ���������� �� Z
	float SpawnZ = 25.f;

	float FoodDeley = 2.f;
	float BufferTimeFood;

	void RandomSpawnFood();






	

};
